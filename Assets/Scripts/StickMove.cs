﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickMove : MonoBehaviour
{

    public GUITexture backgroundImage;
    public KinectWrapper.NuiSkeletonPositionIndex TrackedJoint = KinectWrapper.NuiSkeletonPositionIndex.HandRight;
    public GameObject OverlayObject;
    public float smoothFactor = 5f;

    private float distanceToCamera = 10f;

    public Rigidbody rb;

    void Startsss()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdatesss()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement);
    }

    void Startss()
    {
        if (OverlayObject)
        {
            distanceToCamera = (OverlayObject.transform.position - Camera.main.transform.position).magnitude;
        }
        
    }

    
    void Updatesss()
    {
        KinectManager manager = KinectManager.Instance;
        
        if (true)
        {
            Debug.Log("update2");
            //backgroundImage.renderer.material.mainTexture = manager.GetUsersClrTex();
            if (backgroundImage && (backgroundImage.texture == null))
            {
                backgroundImage.texture = manager.GetUsersClrTex();
            }

            //			Vector3 vRight = BottomRight - BottomLeft;
            //			Vector3 vUp = TopLeft - BottomLeft;

            int iJointIndex = (int)TrackedJoint;

            if (manager.IsUserDetected())
            {
                uint userId = manager.GetPlayer1ID();

                if (manager.IsJointTracked(userId, iJointIndex))
                {
                    Debug.Log("Joint tracked " + iJointIndex);
                    Vector3 posJoint = manager.GetRawSkeletonJointPos(userId, iJointIndex);
                    GetComponent<Rigidbody>().position = posJoint;
                   
                }

            }

        }
    }
}
