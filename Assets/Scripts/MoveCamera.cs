﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    private int headJoint = (int)KinectWrapper.NuiSkeletonPositionIndex.Head;
    private int rightHandJoint = (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight;
    private int leftHandJoint = (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft;
    public float kinectCameraHeight = 0.5f;
    private float movingSpeed = 0.05f;
    private float maxX = 1.7f;
    private float maxZ = 2.5f;
    private float minX = -1.7f;
    private float minZ = 0f;

    private GameObject centerBall;

    private Vector3 cameraPosition = new Vector3(0, 0, 0);

    // Use this for initialization
    void Start () {
        //transform.position = cameraPosition;
        centerBall = GameObject.Find("CenterBall");
        
	}
	
	// Update is called once per frame
	void Update () {

        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized())
        {

            if (manager.IsUserDetected())
            {
                uint userId = manager.GetPlayer1ID();

                Vector3 posJoint = manager.GetRawSkeletonJointPos(userId, headJoint);

                if (posJoint != Vector3.zero)
                {
                    posJoint.z = -posJoint.z;
                    posJoint.y += kinectCameraHeight;

                    transform.position = posJoint + cameraPosition;
                }

                // pobieramy pozycje prawej i lewej reki
                Vector3 rightHandpos = manager.GetRawSkeletonJointPos(userId, rightHandJoint);
                Vector3 leftHandPos = manager.GetRawSkeletonJointPos(userId, leftHandJoint);

                //jesli prawa jest w gorze, ruszamy w prawo
                if (rightHandpos.y > 0.3)
                {
                    float x = cameraPosition.x;
                    float z = cameraPosition.z;

                    if (x > maxX) x = maxX;
                    if (x < minX) x = minX;
                    if (z > maxZ) z = maxZ;
                    if (z < minZ) z = minZ;

                    if (x == maxX)
                    {
                        if (z < maxZ)
                        {
                            cameraPosition.z += movingSpeed;
                        }else
                        {
                            cameraPosition.x -= movingSpeed;
                        }
                        
                    }else if (x == minX)
                    {
                        if (z > minZ)
                        {
                               cameraPosition.z -= movingSpeed;
                        }else
                        {
                               cameraPosition.x += movingSpeed;
                        }
                        
                    }else
                    {
                        if (z == maxZ)
                        {
                            cameraPosition.x -= movingSpeed;
                        }else if (z == minZ)
                        {
                            cameraPosition.x += movingSpeed;
                        }else
                        {
                            cameraPosition.x += movingSpeed;
                        }
                    }
                }
                //jesli lewa w gorze - ruszamy w lewo
                else if (leftHandPos.y > 0.3)
                {
                    float x = cameraPosition.x;
                    float z = cameraPosition.z;

                    if (x > maxX) x = maxX;
                    if (x < minX) x = minX;
                    if (z > maxZ) z = maxZ;
                    if (z < minZ) z = minZ;

                    if (x == maxX)
                    {
                        if (z > minZ)
                        {
                            cameraPosition.z -= movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x -= movingSpeed;
                        }

                    }
                    else if (x == minX)
                    {
                        if (z < maxZ)
                        {
                            cameraPosition.z += movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x += movingSpeed;
                        }

                    }
                    else
                    {
                        if (z == maxZ)
                        {
                            cameraPosition.x += movingSpeed;
                        }
                        else if (z == minZ)
                        {
                            cameraPosition.x -= movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x -= movingSpeed;
                        }
                    }
                }
            }

        }

        transform.LookAt(centerBall.transform.position); //TODO tymczasowo patrzymy na srodek stolu, docelowo to bedzie biala bila
    }
}
