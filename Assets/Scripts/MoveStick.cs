﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveStick : MonoBehaviour {

    private int rightHandJoint = (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight;
    private int leftHandJoint = (int) KinectWrapper.NuiSkeletonPositionIndex.HandLeft;
    private float movingSpeed = 0.05f;
    private float maxX = 1.7f;
    private float maxZ = 2.5f;
    private float minX = -1.7f;
    private float minZ = 0f;

    private Vector3 cameraPosition = new Vector3(0, 0, 0);

    void Update()
    {
        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized())
        {

            if (manager.IsUserDetected())
            {
                uint userId = manager.GetPlayer1ID();

                Vector3 rightHandpos = manager.GetRawSkeletonJointPos(userId, rightHandJoint);

                if (rightHandpos != Vector3.zero)
                {
                    rightHandpos.z = -rightHandpos.z;

                    transform.position = rightHandpos + cameraPosition;
                }

                Vector3 leftHandPos = manager.GetRawSkeletonJointPos(userId, leftHandJoint);

                if (leftHandPos != Vector3.zero)
                {
                    leftHandPos.z = -(leftHandPos.z);

                    transform.LookAt(leftHandPos);
                }

                //jesli prawa jest w gorze, ruszamy w prawo
                if (rightHandpos.y > 0.3)
                {
                    float x = cameraPosition.x;
                    float z = cameraPosition.z;

                    if (x > maxX) x = maxX;
                    if (x < minX) x = minX;
                    if (z > maxZ) z = maxZ;
                    if (z < minZ) z = minZ;

                    if (x == maxX)
                    {
                        if (z < maxZ)
                        {
                            cameraPosition.z += movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x -= movingSpeed;
                        }

                    }
                    else if (x == minX)
                    {
                        if (z > minZ)
                        {
                            cameraPosition.z -= movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x += movingSpeed;
                        }

                    }
                    else
                    {
                        if (z == maxZ)
                        {
                            cameraPosition.x -= movingSpeed;
                        }
                        else if (z == minZ)
                        {
                            cameraPosition.x += movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x += movingSpeed;
                        }
                    }
                }
                //jesli lewa w gorze - ruszamy w lewo
                else if (leftHandPos.y > 0.3)
                {
                    float x = cameraPosition.x;
                    float z = cameraPosition.z;

                    if (x > maxX) x = maxX;
                    if (x < minX) x = minX;
                    if (z > maxZ) z = maxZ;
                    if (z < minZ) z = minZ;

                    if (x == maxX)
                    {
                        if (z > minZ)
                        {
                            cameraPosition.z -= movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x -= movingSpeed;
                        }

                    }
                    else if (x == minX)
                    {
                        if (z < maxZ)
                        {
                            cameraPosition.z += movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x += movingSpeed;
                        }

                    }
                    else
                    {
                        if (z == maxZ)
                        {
                            cameraPosition.x += movingSpeed;
                        }
                        else if (z == minZ)
                        {
                            cameraPosition.x -= movingSpeed;
                        }
                        else
                        {
                            cameraPosition.x -= movingSpeed;
                        }
                    }
                }
            }

        }
    }
}
